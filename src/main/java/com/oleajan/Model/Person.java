package com.oleajan.Model;

public class Person {

  private Long id = (long) 190128895;
  private String firstName = "Juan Dela";
  private String middleName = null;
  private String lastName = "Kruz";
  private String emailAddress = "JD_Kruz@gmail.com";
  private String mobilePhoneNumber = "09123456789";
  private String company = "Tesing Company";
  private String workCategory = "T";
  private String identificationCardNumber = "xxxxxxxxxxx";
  private String identificationcardType = "xxxxxx";
  private String address = "xxx-xxxx-xxxxxxxxxxx";
  private String documentsPresented = null;

  public Long getId() {
      return id;
  }

  public void setId(final Long id) {
      this.id = id;
  }

  public String getFirstName() {
      return firstName;
  }

  public void setFirstName(final String firstName) {
      this.firstName = firstName;
  }

  public String getMiddleName() {
      return middleName;
  }

  public void setMiddleName(final String middleName) {
      this.middleName = middleName;
  }

  public String getLastName() {
      return lastName;
  }

  public void setLastName(final String lastName) {
      this.lastName = lastName;
  }

  public String getEmailAddress() {
      return emailAddress;
  }

  public void setEmailAddress(final String emailAddress) {
      this.emailAddress = emailAddress;
  }

  public String getMobilePhoneNumber() {
      return mobilePhoneNumber;
  }

  public void setMobilePhoneNumber(final String mobilePhoneNumber) {
      this.mobilePhoneNumber = mobilePhoneNumber;
  }

  public String getCompany() {
      return company;
  }

  public void setCompany(final String company) {
      this.company = company;
  }

  public String getWorkCategory() {
      return workCategory;
  }

  public void setWorkCategory(final String workCategory) {
      this.workCategory = workCategory;
  }

  public String getIdentificationCardNumber() {
      return identificationCardNumber;
  }

  public void setIdentificationCardNumber(final String identificationCardNumber) {
      this.identificationCardNumber = identificationCardNumber;
  }

  public String getIdentificationcardType() {
      return identificationcardType;
  }

  public void setIdentificationcardType(final String identificationcardType) {
      this.identificationcardType = identificationcardType;
  }

  public String getAddress() {
      return address;
  }

  public void setAddress(final String address) {
      this.address = address;
  }

  public String getDocumentsPresented() {
      return documentsPresented;
  }

  public void setDocumentsPresented(final String documentsPresented) {
      this.documentsPresented = documentsPresented;
  }


}
