package com.oleajan.Model;

public class VehicleRapidPass {
    private Long id = (long)45678123;
    private String accessType = "XXX";
    private String origin = "xxxxxxx";
    private String destinations = "yyyyyyyy";
    private String checkpoints = "xxxxxx";
    private String authorizedDriver = "Juan Dela Kruz";
    private Vehicle vehicle = new Vehicle();
    private String additionalCrewMembers = "xx";
    private String passRequestResult = "xxxxx";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestinations() {
        return destinations;
    }

    public void setDestinations(String destinations) {
        this.destinations = destinations;
    }

    public String getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(String checkpoints) {
        this.checkpoints = checkpoints;
    }

    public String getAuthorizedDriver() {
        return authorizedDriver;
    }

    public void setAuthorizedDriver(String authorizedDriver) {
        this.authorizedDriver = authorizedDriver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getAdditionalCrewMembers() {
        return additionalCrewMembers;
    }

    public void setAdditionalCrewMembers(String additionalCrewMembers) {
        this.additionalCrewMembers = additionalCrewMembers;
    }

    public String getPassRequestResult() {
        return passRequestResult;
    }

    public void setPassRequestResult(String passRequestResult) {
        this.passRequestResult = passRequestResult;
    }
}
