package com.oleajan;

import com.oleajan.Model.Person;
import com.oleajan.Model.VehicleRapidPass;

import java.io.FileNotFoundException;

/**
 * Prototype PDF Generator for DCTx Rapidpass
 *
 */
public class App 
{
    public static void main( String[] args ) {
        PDFGenerator pdfGenerator = new PDFGenerator();
//        pdfGenerator.generateIndividualPDF("resources/CxEncSerializedPass.png", new Person());
        pdfGenerator.generateVehiclePdf("resources/CxEncSerializedPass.png");
    }
}
